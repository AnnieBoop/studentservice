package service.interfaces;

import java.util.List;

import dto.PersonDTO;
import dto.StudentDTO;

public interface IStudentInterface {
	public List<StudentDTO> listStudents();
	public boolean addStudent(StudentDTO studentDTO);
}
