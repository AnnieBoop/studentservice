package controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import dto.PersonDTO;
import dto.ProfessorDTO;
import dto.StudentDTO;
import dto.TownDTO;
import dto.UniversityDTO;
import service.interfaces.IProfessorService;
import service.interfaces.IStudentInterface;
import service.interfaces.ITownInterface;
import service.interfaces.IUniversityService;

@RestController
@RequestMapping("api/service")
public class MyRestController {
	
	@Autowired
	IStudentInterface studentInterface;
	
	@Autowired
	IProfessorService professorService;
	
	@Autowired
	IUniversityService universityService;
	
	@Autowired
	ITownInterface townInterface;
	
	@RequestMapping("/students")
	public List<StudentDTO> listStudents(){
		return studentInterface.listStudents();
	}
	
	@RequestMapping("/towns")
	public List<TownDTO> listTowns(){
		return townInterface.listTowns();
	}
	
	@RequestMapping("/towns/{id}")
	public TownDTO findTown(@PathVariable("id") int id) {
		return townInterface.findTownById(id);
	}
	
	@RequestMapping("/towns/{id}/universities")
	public TownDTO listTownsUniversities(@PathVariable("id") int id) {
		return townInterface.listTownsUniversities(id);
	}
	
	@RequestMapping(path = "/towns/add", method = RequestMethod.POST)
	public TownDTO addTown(
			@RequestBody
			TownDTO townDTO) {
		townInterface.addTown(townDTO);
		return townDTO;
	}
	
	@RequestMapping("/towns/delete/{id}")
	public boolean deleteTown(@PathVariable("id") int id) {
		townInterface.removeTown(id);
		return true;
	}
	
	@RequestMapping(path = "/towns/update/{id}", method = RequestMethod.POST)
	public TownDTO updateTown(@PathVariable("id") int id, @RequestBody TownDTO townDTO) {
		townInterface.updateTown(id, townDTO);
		return townDTO;
	}
	
	@RequestMapping("/professors")
	public List<ProfessorDTO> listProfessors(){
		return professorService.listProfessors();
	}
	
	@RequestMapping("/universities")
	public List<UniversityDTO> listUniversities(){
		return universityService.listUniversities();
	}
	
	@RequestMapping(path = "/universities/add", method = RequestMethod.POST)
	public UniversityDTO addUniversity(@RequestBody UniversityDTO universityDTO) {
		universityService.addUniversity(universityDTO);
		return universityDTO;
	}
}
