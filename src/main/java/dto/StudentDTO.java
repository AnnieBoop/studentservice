package dto;

import java.util.List;

import model.Person;
import model.Student;
import model.Subject;
import model.Test;
import model.University;

public class StudentDTO {
	private int studentId;
	
	private String hobby;
	
	private String sport;
	
	private PersonDTO personDTO;
	
	private SubjectDTO subjectDTO;
	
	private TestDTO testDTO;
	
	private UniversityDTO universityDTO;
	
	public UniversityDTO getUniversityDTO() {
		return universityDTO;
	}

	public void setUniversityDTO(UniversityDTO universityDTO) {
		this.universityDTO = universityDTO;
	}

	public TestDTO getTestDTO() {
		return testDTO;
	}

	public void setTestDTO(TestDTO testDTO) {
		this.testDTO = testDTO;
	}

	public SubjectDTO getSubjectDTO() {
		return subjectDTO;
	}

	public void setSubjectDTO(SubjectDTO subjectDTO) {
		this.subjectDTO = subjectDTO;
	}

	public PersonDTO getPersonDTO() {
		return personDTO;
	}

	public void setPersonDTO(PersonDTO personDTO) {
		this.personDTO = personDTO;
	}

	@Override
	public String toString() {
		return "StudentDTO [studentId=" + studentId + ", hobby=" + hobby + ", sport=" + sport + "]";
	}

	public int getStudentId() {
		return studentId;
	}

	public void setStudentId(int studentId) {
		this.studentId = studentId;
	}

	public String getHobby() {
		return hobby;
	}

	public void setHobby(String hobby) {
		this.hobby = hobby;
	}

	public String getSport() {
		return sport;
	}

	public void setSport(String sport) {
		this.sport = sport;
	}

	public StudentDTO(int studentId, String hobby, String sport) {
		super();
		this.studentId = studentId;
		this.hobby = hobby;
		this.sport = sport;
	}

	public StudentDTO() {
		super();
	}
	
	public StudentDTO(Student student) {
		
		setPersonDTO(new PersonDTO(student.getPerson()));
		setSubjectDTO(new SubjectDTO(student.getSubject()));
		setTestDTO(new TestDTO(student.getTest()));
		setUniversityDTO(new UniversityDTO(student.getUniversity()));
		setStudentId(student.getStudentId());
		setHobby(student.getHobby());
		setSport(student.getSport());
	}
}
