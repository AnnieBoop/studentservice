package repository;

import org.springframework.data.jpa.repository.JpaRepository;

import model.University;

public interface UniversityRepository extends JpaRepository<University, Integer>{

}
