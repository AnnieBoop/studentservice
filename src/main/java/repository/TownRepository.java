package repository;

import org.springframework.data.jpa.repository.JpaRepository;

import model.Town;

public interface TownRepository extends JpaRepository<Town, Integer>{

}
